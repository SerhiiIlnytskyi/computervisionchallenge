## Introduction
A company called IR (Impressive Reality) is going to release AR platform, so some impressive functionality has to be developed.
The main goal of the challenge is to provide Lipstick Showroom functionality.
Basically, there will be a need to “apply” a selected lipstick in an augmented reality fashion, in order to improve customer experience.

## Complexity levels

### Minimal complexity

This is the basic level, so the only need will be to replace a color within a given shape.

### Medium complexity
Same as the minimal, but the shape provided will be rough, so additional work will be needed to apply new color properly.

### High complexity
Main challenge is here. Challenge Team will provide only bounding boxes, so it will up to developers/teams to implement proper segmentation and color replacement.

## Tools & Utils
Initial image annotations in the XML format will be provided to the challengers.
The image processing tool can be used to parse the annotation files and to visualize processing results.


*Usage:*

  ```bash
  java -jar computer-vision-challenge-0.0.1.jar [-h] [--ui] -a=<annotationxml> -o=<output>
      -h, --help                          display a help message
      -a, --annotation=<annotationxml>    xml file containing image annotations
      -o, --output=<output>               annotated image output path
      --ui                                show image preview
```
e.g.: `java -jar computer-vision-challenge-0.0.1.jar -a annotated-image.xml -o lena.result.jpg`


**Please note. **The path to the original image is specified inside the annotation file that's why you'll probably need to modify the image path in the provided XML.


In order to solve challenge tasks, you'll need to add custom AnnotationProcessor or modify existing one.
