package com.epam.vision.image;

import com.epam.vision.image.annotations.BoundingBox;
import com.epam.vision.image.annotations.ImageAnnotation;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Objects;

public class BoundingBoxAnnotationProcessor implements AnnotationProcessor {

  @Override
  public boolean canBeApplied(ImageAnnotation annotation) {
    return annotation instanceof BoundingBox;
  }

  @Override
  public void apply(BufferedImage image, ImageAnnotation annotation) {
    Objects.requireNonNull(image, "Image is required");
    BoundingBox bbAnnotation = (BoundingBox) annotation;
    Graphics2D g2d = image.createGraphics();
    g2d.setColor(bbAnnotation.getRectColor());
    int x = bbAnnotation.getxMin();
    int y = bbAnnotation.getyMin();
    int width = bbAnnotation.getxMax() - x;
    int height = bbAnnotation.getyMax() - y;
    g2d.drawRect(x, y, width, height);
    g2d.dispose();
  }
}
