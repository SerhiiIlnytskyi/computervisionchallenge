package com.epam.vision.image.context;

import com.epam.vision.image.annotations.Mask;
import com.epam.vision.image.model.Pixel;
import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Serhii Ilnytskyi
 * @version 1.0
 * @since 30.11.2019
 */

public class MaskAnnotationContext extends AnnotationContext<Mask> {

  private Set<Pixel> pixels = new HashSet<>();
  private float[] averageColorCieLab;

  public MaskAnnotationContext(BufferedImage image, Mask imageAnnotation) {
    super(image, imageAnnotation);
  }

  public Set<Pixel> getPixels() {
    return pixels;
  }

  public void setPixels(Set<Pixel> pixels) {
    this.pixels = pixels;
  }

  public float[] getAverageColorCieLab() {
    return averageColorCieLab;
  }

  public void setAverageColorCieLab(float[] averageColorCieLab) {
    this.averageColorCieLab = averageColorCieLab;
  }
}
