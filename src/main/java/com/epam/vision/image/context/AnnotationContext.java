package com.epam.vision.image.context;

import com.epam.vision.image.annotations.ImageAnnotation;
import java.awt.image.BufferedImage;

/**
 * @author Serhii Ilnytskyi
 * @version 1.0
 * @since 30.11.2019
 */

public class AnnotationContext<IA extends ImageAnnotation> extends ImageContext {

  private IA imageAnnotation;

  public AnnotationContext(BufferedImage image, IA imageAnnotation) {
    super(image);
    this.imageAnnotation = imageAnnotation;
  }

  public IA getImageAnnotation() {
    return imageAnnotation;
  }

  public void setImageAnnotation(IA imageAnnotation) {
    this.imageAnnotation = imageAnnotation;
  }
}
