package com.epam.vision.image.context;

import java.awt.image.BufferedImage;

/**
 * @author Serhii Ilnytskyi
 * @version 1.0
 * @since 30.11.2019
 */

public class ImageContext {

  protected BufferedImage image;

  public ImageContext(BufferedImage image) {
    this.image = image;
  }

  public BufferedImage getImage() {
    return image;
  }

  public void setImage(BufferedImage image) {
    this.image = image;
  }
}
