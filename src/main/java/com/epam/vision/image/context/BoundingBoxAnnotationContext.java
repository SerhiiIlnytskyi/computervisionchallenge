package com.epam.vision.image.context;

import com.epam.vision.image.annotations.BoundingBox;
import java.awt.image.BufferedImage;

/**
 * @author Serhii Ilnytskyi
 * @version 1.0
 * @since 08.12.2019
 */

public class BoundingBoxAnnotationContext extends AnnotationContext<BoundingBox>{

  public BoundingBoxAnnotationContext(BufferedImage image,
      BoundingBox imageAnnotation) {
    super(image, imageAnnotation);
  }
}
