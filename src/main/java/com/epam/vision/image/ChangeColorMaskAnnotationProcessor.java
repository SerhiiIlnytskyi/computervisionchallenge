package com.epam.vision.image;

import com.epam.vision.image.annotations.ImageAnnotation;
import com.epam.vision.image.annotations.Mask;
import com.epam.vision.image.context.AnnotationContext;
import com.epam.vision.image.context.MaskAnnotationContext;
import com.epam.vision.image.stage.FloodFillStage;
import com.epam.vision.image.context.ImageContext;
import com.epam.vision.image.stage.MaskInitializationStage;
import com.epam.vision.image.pipeline.Pipeline;
import java.awt.image.BufferedImage;
import java.util.Objects;

public class ChangeColorMaskAnnotationProcessor implements AnnotationProcessor {

  @Override
  public boolean canBeApplied(ImageAnnotation annotation) {
    return annotation instanceof Mask;
  }

  @Override
  public void apply(BufferedImage image, ImageAnnotation annotation) {
    Objects.requireNonNull(image, "Image is required");

    MaskAnnotationContext maskAnnotationContext = new MaskAnnotationContext(image,
        (Mask) annotation);

    Pipeline<MaskAnnotationContext, ImageContext> pipeline = new Pipeline<>(
        new MaskInitializationStage())
        .pipe(new FloodFillStage());

    pipeline.execute(maskAnnotationContext);

  }
}
