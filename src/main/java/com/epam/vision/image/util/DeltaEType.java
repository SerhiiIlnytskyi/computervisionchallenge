package com.epam.vision.image.util;

/**
 * @author Serhii Ilnytskyi
 * @version 1.0
 * @since 25.11.2019
 */

public enum DeltaEType {
  DELTA_RGB_EUCLID, DELTA_E_1994(1.0, 1.0), DELTA_E_2000(1.0, 1.0, 1.0),
  ;

  private double whL;
  private double whC;
  private double whH;
  private double Kc;
  private double Kh;

  DeltaEType() {
  }

  DeltaEType(double whL, double whC, double whH) {
    this.whL = whL;
    this.whC = whC;
    this.whH = whH;
  }

  DeltaEType(double Kc, double Kh) {
    this.Kc = Kc;
    this.Kh = Kh;
  }

  public void setWeights(double whL, double whC, double whH) {
    this.whL = whL;
    this.whC = whC;
    this.whH = whH;
  }

  public void setWeights(double Kc, double Kh) {
    this.Kc = Kc;
    this.Kh = Kh;
  }

  public double getWhL() {
    return whL;
  }

  public double getWhC() {
    return whC;
  }

  public double getWhH() {
    return whH;
  }

  public double getWhKc() {
    return Kc;
  }

  public double getWhKh() {
    return Kh;
  }
}
