package com.epam.vision.image.util;

import java.awt.Color;

/**
 * @author Serhii Ilnytskyi
 * @version 1.0
 * @since 25.11.2019
 */

public class DeltaE {

  private static final CIELab cielabConverter = CIELab.getInstance();

  private DeltaEType deltaEType;

  public DeltaE(DeltaEType deltaEType) {
    this.deltaEType = deltaEType;
  }

  public double calculate(Color color1, Color color2) {
    return calculate(color1.getRGB(), color2.getRGB());
  }

  public double calculate(float[] cieLab1, float[] cieLab2) {
    if (deltaEType == DeltaEType.DELTA_E_2000) {
      return calculateDeltaE_2000(cieLab1, cieLab2);
    } else if (deltaEType == DeltaEType.DELTA_E_1994) {
      return calculateDeltaE_1994(cieLab1, cieLab2);
    } else {
      return 0;//todo
    }
  }

  public double calculate(int[] rgb1, int[] rgb2) {
    if (deltaEType == DeltaEType.DELTA_RGB_EUCLID) {
      return calculateDeltaERgbEuclid(rgb1, rgb2);
    }

    float[] cieLab1 = cielabConverter
        .fromRGB(new float[]{rgb1[0] / 256f, rgb1[1] / 256f, rgb1[2] / 256f});
    float[] cieLab2 = cielabConverter
        .fromRGB(new float[]{rgb2[0] / 256f, rgb2[1] / 256f, rgb2[2] / 256f});
    return calculate(cieLab1, cieLab2);
  }

  public double calculate(int rgb1, int rgb2) {
    int red1 = (rgb1 >> 16) & 0xFF;
    int green1 = (rgb1 >> 8) & 0xFF;
    int blue1 = rgb1 & 0xFF;
    int red2 = (rgb2 >> 16) & 0xFF;
    int green2 = (rgb2 >> 8) & 0xFF;
    int blue2 = rgb2 & 0xFF;
    return calculate(new int[]{red1, green1, blue1}, new int[]{red2, green2, blue2});
  }

  private double calculateDeltaERgbEuclid(int[] rgb1, int[] rgb2) {
    return Math.sqrt((rgb2[0] - rgb1[0]) * (rgb2[0] - rgb1[0]) + (rgb2[1] - rgb1[1]) * (rgb2[1]
        - rgb1[1]) + (rgb2[2] - rgb1[2]) * (rgb2[2] - rgb1[2]));
  }

  private double calculateDeltaE_1994(float[] cieLab1, float[] cieLab2) {
    double deltaL = cieLab1[0] - cieLab2[0];
    double C1 = Math.sqrt(cieLab1[1] * cieLab1[1] + cieLab1[2] * cieLab1[2]);
    double C2 = Math.sqrt(cieLab2[1] * cieLab2[1] + cieLab2[2] * cieLab2[2]);
    double deltaC = C1 - C2;
    double deltaa = cieLab1[1] - cieLab2[1];
    double deltab = cieLab1[2] - cieLab2[2];
    double deltaH = Math.sqrt(deltaa * deltaa + deltab * deltab - deltaC * deltaC);
    double Kl = 1; //2 if textiles
    double K1 = 0.045; // 0.048 if textiles
    double K2 = 0.015; // 0.014 if textiles
    double Sl = 1;
    double Sc = 1 + K1 * C1;
    double Sh = 1 + K2 * C1;
    double deltaE94 = Math.sqrt(
        Math.pow(deltaL / (Kl * Sl), 2) + Math.pow(deltaC / (deltaEType.getWhKc() * Sc), 2)
            + Math.pow(deltaH / (deltaEType.getWhKh() * Sh), 2));
    return deltaE94;
  }

  private double calculateDeltaE_2000(float[] cieLab1, float[] cieLab2) {
    double Lmean = (cieLab1[0] + cieLab2[0]) / 2.0;
    double C1 = Math.sqrt(cieLab1[1] * cieLab1[1] + cieLab1[2] * cieLab1[2]);
    double C2 = Math.sqrt(cieLab2[1] * cieLab2[1] + cieLab2[2] * cieLab2[2]);
    double Cmean = (C1 + C2) / 2.0;

    double G = (1 - Math.sqrt(Math.pow(Cmean, 7) / (Math.pow(Cmean, 7) + Math.pow(25, 7))))
        / 2;
    double a1prime = cieLab1[1] * (1 + G);
    double a2prime = cieLab2[1] * (1 + G);

    double C1prime = Math.sqrt(a1prime * a1prime + cieLab1[2] * cieLab1[2]);
    double C2prime = Math.sqrt(a2prime * a2prime + cieLab2[2] * cieLab2[2]);
    double Cmeanprime = (C1prime + C2prime) / 2;

    double h1prime =
        Math.atan2(cieLab1[2], a1prime) + 2 * Math.PI * (Math.atan2(cieLab1[2], a1prime) < 0 ? 1
            : 0);
    double h2prime =
        Math.atan2(cieLab2[2], a2prime) + 2 * Math.PI * (Math.atan2(cieLab2[2], a2prime) < 0 ? 1
            : 0);
    double Hmeanprime = ((Math.abs(h1prime - h2prime) > Math.PI) ?
        (h1prime + h2prime + 2 * Math.PI) / 2 :
        (h1prime + h2prime) / 2);

    double T =
        1.0 - 0.17 * Math.cos(Hmeanprime - Math.PI / 6.0) + 0.24 * Math.cos(2 * Hmeanprime)
            + 0.32 * Math.cos(3 * Hmeanprime + Math.PI / 30) - 0.2 * Math
            .cos(4 * Hmeanprime - 21 * Math.PI / 60);

    double deltahprime = ((Math.abs(h1prime - h2prime) <= Math.PI) ?
        h2prime - h1prime :
        (h2prime <= h1prime) ?
            h2prime - h1prime + 2 * Math.PI :
            h2prime - h1prime - 2 * Math.PI);

    double deltaLprime = cieLab2[0] - cieLab1[0];
    double deltaCprime = C2prime - C1prime;
    double deltaHprime = 2.0 * Math.sqrt(C1prime * C2prime) * Math.sin(deltahprime / 2.0);
    double SL = 1.0 + ((0.015 * (Lmean - 50) * (Lmean - 50)) / (Math
        .sqrt(20 + (Lmean - 50) * (Lmean - 50))));
    double SC = 1.0 + 0.045 * Cmeanprime;
    double SH = 1.0 + 0.015 * Cmeanprime * T;

    double deltaTheta = (30 * Math.PI / 180) * Math
        .exp(-((180 / Math.PI * Hmeanprime - 275) / 25) * (
            (180 / Math.PI * Hmeanprime - 275) / 25));
    double RC = (2 * Math
        .sqrt(Math.pow(Cmeanprime, 7) / (Math.pow(Cmeanprime, 7) + Math.pow(25, 7))));
    double RT = (-RC * Math.sin(2 * deltaTheta));

    double KL = deltaEType.getWhL();
    double KC = deltaEType.getWhC();
    double KH = deltaEType.getWhH();
    double deltaE = Math.sqrt(
        ((deltaLprime / (KL * SL)) * (deltaLprime / (KL * SL))) + ((deltaCprime / (KC * SC))
            * (deltaCprime / (KC * SC))) + ((deltaHprime / (KH * SH)) * (deltaHprime / (
            KH * SH))) + (RT * (deltaCprime / (KC * SC)) * (deltaHprime / (KH * SH))));

    return deltaE;
  }
}

