package com.epam.vision.image.util;

import java.awt.color.ColorSpace;

public class CIELab extends ColorSpace {

  private static final double SIGMA = 4.0 / 29.0;

  public static CIELab getInstance() {
    return Holder.INSTANCE;
  }

  CIELab() {
    super(ColorSpace.TYPE_Lab, 3);
  }

  private static class Holder {

    static final CIELab INSTANCE = new CIELab();

  }

  private static final ColorSpace CIEXYZ =
      ColorSpace.getInstance(ColorSpace.CS_CIEXYZ);

  public float[] fromRGB(int rgb) {
    int red = (rgb >> 16) & 0xFF;
    int green = (rgb >> 8) & 0xFF;
    int blue = rgb & 0xFF;
    return fromRGB(new float[]{red / 256f, green / 256f, blue / 256f});
  }

  @Override
  public float[] fromRGB(float[] rgbValue) {
    float[] xyz = CIEXYZ.fromRGB(rgbValue);
    return fromCIEXYZ(xyz);
  }

  @Override
  public float[] toRGB(float[] cieLabValue) {
    float[] xyz = toCIEXYZ(cieLabValue);
    return CIEXYZ.toRGB(xyz);
  }

  @Override
  public float[] fromCIEXYZ(float[] cieXYZValue) {
    double l = f(cieXYZValue[1]);
    double L = 116.0 * l - 16.0;
    double a = 500.0 * (f(cieXYZValue[0]) - l);
    double b = 200.0 * (l - f(cieXYZValue[2]));
    return new float[]{(float) L, (float) a, (float) b};
  }

  private double f(double t) {
    if (t > 216.0 / 24389.0) {
      return Math.cbrt(t);
    } else {
      return (841.0 / 108.0) * t + SIGMA;
    }
  }

  @Override
  public float[] toCIEXYZ(float[] cieLabValue) {
    double i = (cieLabValue[0] + 16.0) * (1.0 / 116.0);
    double X = fInv(i + cieLabValue[1] * (1.0 / 500.0));
    double Y = fInv(i);
    double Z = fInv(i - cieLabValue[2] * (1.0 / 200.0));
    return new float[]{(float) X, (float) Y, (float) Z};
  }

  private double fInv(double t) {
    if (t > 6.0 / 29.0) {
      return t * t * t;
    } else {
      return (108.0 / 841.0) * (t - SIGMA);
    }
  }

  @Override
  public float getMaxValue(int component) {
    return 128f;
  }

  @Override
  public float getMinValue(int component) {
    return (component == 0) ? 0f : -128f;
  }

  @Override
  public String getName(int idx) {
    return String.valueOf("Lab".charAt(idx));
  }
}
