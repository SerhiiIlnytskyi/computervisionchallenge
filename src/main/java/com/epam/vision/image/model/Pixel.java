package com.epam.vision.image.model;

import com.epam.vision.image.util.CIELab;
import java.awt.Point;
import java.awt.geom.Point2D;

/**
 * @author Serhii Ilnytskyi
 * @version 1.0
 * @since 30.11.2019
 */

public class Pixel {

  private static final CIELab CIELAB_CONVERTER = CIELab.getInstance();


  private int x;
  private int y;
  private int rgb;
  private float[] cieLab;

  public Pixel(Point2D point2D, float[] cieLab) {
    this.cieLab = cieLab;
    float[] rgbFloat = CIELAB_CONVERTER.toRGB(cieLab);
    rgb = (int) (rgbFloat[0] * 256);
    rgb = (rgb << 8) + (int) (rgbFloat[1] * 256);
    rgb = (rgb << 8) + (int) (rgbFloat[2] * 256);
    this.x = (int) point2D.getX();
    this.y = (int) point2D.getY();
  }

  public Pixel(int x, int y, int rgb) {
    this.rgb = rgb;
    this.cieLab = CIELAB_CONVERTER.fromRGB(rgb);
    this.x = x;
    this.y = y;
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  public float[] getCieLab() {
    return cieLab;
  }

  public void setCieLab(float[] cieLab) {
    this.cieLab = cieLab;
  }

  public int getRgb() {
    return rgb;
  }

  public void setRgb(int rgb) {
    this.rgb = rgb;
  }

  public int getRed() {
    return (rgb >> 16) & 0xFF;
  }

  public int getGreen() {
    return (rgb >> 8) & 0xFF;
  }

  public int getBlue() {
    return rgb & 0xFF;
  }

  public int getY_YCbCr() {
    return (int) (0 + (0.299 * getRed()) + (0.587 * getGreen()) + (0.114 * getBlue()));
  }

  public int getCb_YCbCr() {
    return (int) (128 - (0.168736 * getRed()) - (0.331264 * getGreen()) + (0.5 * getBlue()));
  }

  public int getCr_YCbCr() {
    return (int) (128 + (0.5 * getRed()) - (0.418688 * getGreen()) - (0.081312 * getBlue()));
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Pixel pixel = (Pixel) o;
    return x == pixel.x && y == pixel.y;
  }

  @Override
  public int hashCode() {
    return com.google.common.base.Objects.hashCode(x, y);
  }
}
