package com.epam.vision.image;

import com.epam.vision.image.annotations.ImageAnnotation;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AnnotatedImage {

  private static final Logger logger = LogManager.getLogger(AnnotatedImage.class);
  private String imageFile;
  private List<ImageAnnotation> annotations = new LinkedList<>();

  public AnnotatedImage() {
  }

  public AnnotatedImage(String imageFile) {
    this.imageFile = imageFile;
  }

  public void setImageFile(String imageFile) {
    this.imageFile = imageFile;
  }

  public void addAnnotation(ImageAnnotation annotation) {
    if (null != annotation) {
      annotations.add(annotation);
    }
  }


  public String getImageFile() {
    return imageFile;
  }

  public List<ImageAnnotation> getAnnotations() {
    return annotations;
  }

  public void setAnnotations(List<ImageAnnotation> annotations) {
    this.annotations = annotations;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnnotatedImage image = (AnnotatedImage) o;
    return Objects.equals(imageFile, image.imageFile) &&
        Objects.equals(annotations, image.annotations);
  }


  @Override
  public int hashCode() {
    return Objects.hash(imageFile, annotations);
  }

  @Override
  public String toString() {
    return "AnnotatedImage{" +
        "imageFile='" + imageFile + '\'' +
        ", annotations=" + annotations +
        '}';
  }
}
