package com.epam.vision.image;

import com.epam.vision.image.annotations.BoundingBox;
import com.epam.vision.image.annotations.ImageAnnotation;
import com.epam.vision.image.annotations.Mask;
import com.epam.vision.image.context.BoundingBoxAnnotationContext;
import com.epam.vision.image.context.ImageContext;
import com.epam.vision.image.model.Pixel;
import com.epam.vision.image.pipeline.Pipeline;
import com.epam.vision.image.stage.FloodFillStage;
import com.epam.vision.image.stage.MaskInitializationStage;
import com.epam.vision.image.stage.segmentation.ThreasholdingStage2;
import java.awt.image.BufferedImage;
import java.util.Objects;
import java.util.function.Function;

public class ThreasholdingAnnotationProcessor implements AnnotationProcessor {

  @Override
  public boolean canBeApplied(ImageAnnotation annotation) {
    return annotation instanceof BoundingBox || annotation instanceof Mask;
  }

  @Override
  public void apply(BufferedImage image, ImageAnnotation annotation) {
    Objects.requireNonNull(image, "Image is required");
    BoundingBox bbAnnotation = (BoundingBox) annotation;

    BoundingBoxAnnotationContext boundingBoxAnnotationContext = new BoundingBoxAnnotationContext(
        image, bbAnnotation);

    Function<Pixel, Integer> function = pixel -> {
      if ((pixel.getRed() > 180) && (pixel.getGreen() > 60)) {
        return 2;
      }
      return 0;
    };

    Pipeline<BoundingBoxAnnotationContext, ImageContext> pipeline = new Pipeline<>(
        new ThreasholdingStage2(function, 1)).pipe(new MaskInitializationStage())
        .pipe(new FloodFillStage());

    pipeline.execute(boundingBoxAnnotationContext);
  }
}
