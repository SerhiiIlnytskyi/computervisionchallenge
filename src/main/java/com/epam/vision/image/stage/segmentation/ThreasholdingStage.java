package com.epam.vision.image.stage.segmentation;

import com.epam.vision.image.annotations.BoundingBox;
import com.epam.vision.image.annotations.Mask;
import com.epam.vision.image.context.BoundingBoxAnnotationContext;
import com.epam.vision.image.context.MaskAnnotationContext;
import com.epam.vision.image.model.Pixel;
import com.epam.vision.image.stage.Stage;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;
import java.util.function.Function;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Serhii Ilnytskyi
 * @version 1.0
 * @since 08.12.2019
 */

public class ThreasholdingStage implements Stage<BoundingBoxAnnotationContext, MaskAnnotationContext> {

  private static final Logger logger = LogManager.getLogger(ThreasholdingStage.class);
  private final Integer T;

  private Function<Pixel, Integer> threashodlgingFunction;

  public ThreasholdingStage(Function<Pixel, Integer> function, Integer T) {
    this.threashodlgingFunction = function;
    this.T = T;
  }

  @Override
  public MaskAnnotationContext process(BoundingBoxAnnotationContext input) throws StageException {
    BufferedImage image = input.getImage();
    BoundingBox boundingBox = input.getImageAnnotation();
    Shape shape = new Area();

    for (int i = boundingBox.getxMin(); i < boundingBox.getxMax(); i++) {
      for (int j = boundingBox.getyMin(); j < boundingBox.getyMax(); j++) {
        Pixel pixel = new Pixel(i, j, image.getRGB(i, j));
        if (this.threashodlgingFunction.apply(pixel) < this.T) {
          image.setRGB(i, j, 0);
        } else {
          image.setRGB(i, j, 16777215);
          ((Area) shape).add(new Area(new Polygon(new int[]{i}, new int[]{j}, 1)));
        }
      }
    }
    return new MaskAnnotationContext(image, new Mask(shape));
  }
}
