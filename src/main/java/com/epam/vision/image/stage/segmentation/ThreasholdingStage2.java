package com.epam.vision.image.stage.segmentation;

import com.epam.vision.image.annotations.BoundingBox;
import com.epam.vision.image.annotations.Mask;
import com.epam.vision.image.context.AnnotationContext;
import com.epam.vision.image.context.BoundingBoxAnnotationContext;
import com.epam.vision.image.context.MaskAnnotationContext;
import com.epam.vision.image.model.Pixel;
import com.epam.vision.image.stage.Stage;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Area;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Serhii Ilnytskyi
 * @version 1.0
 * @since 08.12.2019
 */

public class ThreasholdingStage2 implements Stage<BoundingBoxAnnotationContext, MaskAnnotationContext> {

  private static final Logger logger = LogManager.getLogger(ThreasholdingStage2.class);
  private final Integer T;

  private Function<Pixel, Integer> threashodlgingFunction;

  public ThreasholdingStage2(Function<Pixel, Integer> function, Integer T) {
    this.threashodlgingFunction = function;
    this.T = T;
  }

  @Override
  public MaskAnnotationContext process(BoundingBoxAnnotationContext input) throws StageException {
    BufferedImage image = input.getImage();
    BoundingBox boundingBox = input.getImageAnnotation();
    Shape shape = new Area();
    Map<Integer, Integer> CbGreaterThan100 = new HashMap<>();
    Map<Integer, Integer> CrGreaterThan150 = new HashMap<>();
    for (int i = boundingBox.getxMin(); i < boundingBox.getxMax(); i++) {
      for (int j = boundingBox.getyMin(); j < boundingBox.getyMax(); j++) {
        Pixel pixel = new Pixel(i, j, image.getRGB(i, j));
        int Cb_YCbCr = pixel.getCb_YCbCr();
        if (Cb_YCbCr > 100) {
          if (CbGreaterThan100.containsKey(Cb_YCbCr)) {
            CbGreaterThan100.put(Cb_YCbCr, CbGreaterThan100.get(Cb_YCbCr) + 1);
          } else {
            CbGreaterThan100.put(Cb_YCbCr, 1);
          }
        }
        int Cr_YCbCr = pixel.getCr_YCbCr();
        if (Cr_YCbCr > 150) {
          if (CrGreaterThan150.containsKey(Cr_YCbCr)) {
            CrGreaterThan150.put(Cr_YCbCr, CrGreaterThan150.get(Cr_YCbCr) + 1);
          } else {
            CrGreaterThan150.put(Cr_YCbCr, 1);
          }
        }


//        if (this.threashodlgingFunction.apply(pixel) < this.T) {
//          image.setRGB(i, j, 0);
//        } else {
//          image.setRGB(i, j, 16777215);
//          ((Area) shape).add(new Area(new Polygon(new int[]{i}, new int[]{j}, 1)));
//        }
      }
    }



    System.out.println(CrGreaterThan150.size());
    System.out.println(CbGreaterThan100.size());
    int N = 0;
    for (Entry<Integer, Integer> entry : CbGreaterThan100.entrySet()) {
      N += entry.getValue();
    }
    int S = 0;
    for (Entry<Integer, Integer> entry : CbGreaterThan100.entrySet()) {
      S += entry.getValue() * entry.getKey();
    }
    double t1 = S * 1.0 / N;

    int N2 = 0;
    for (Entry<Integer, Integer> entry : CrGreaterThan150.entrySet()) {
      N2 += entry.getValue();
    }
    int S2 = 0;
    for (Entry<Integer, Integer> entry : CrGreaterThan150.entrySet()) {
      S2 += entry.getValue() * entry.getKey();
    }
    double t2 = S2 * 1.0 / N2;

    double thresh1 = (t1 + t2) / 2;
    double thresh2 = t2;

    for (int i = boundingBox.getxMin(); i < boundingBox.getxMax(); i++) {
      for (int j = boundingBox.getyMin(); j < boundingBox.getyMax(); j++) {
        Pixel pixel = new Pixel(i, j, image.getRGB(i, j));
        if ((((pixel.getCb_YCbCr() + pixel.getCr_YCbCr()) / 2) > thresh1) && (pixel.getCr_YCbCr() > thresh2)){
          //image.setRGB(i, j, 0);
          Area area = new Area();
          area.add(new Area(new Rectangle(i, j, 1, 1)));

          ((Area) shape).add(area);
        } else {
         // image.setRGB(i, j, 16777215);
        }
      }
    }

    return new MaskAnnotationContext(image, new Mask(shape));
  }
}
