package com.epam.vision.image.stage;

import com.epam.vision.image.context.ImageContext;

/**
 * @author Serhii Ilnytskyi
 * @version 1.0
 * @since 30.11.2019
 */

public interface Stage<I extends ImageContext, O extends ImageContext> {
  public static class StageException extends RuntimeException {
    public StageException(Throwable t) {
      super(t);
    }
  }
  public O process(I input) throws StageException;
}
