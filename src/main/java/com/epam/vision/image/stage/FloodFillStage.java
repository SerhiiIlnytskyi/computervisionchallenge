package com.epam.vision.image.stage;

import com.epam.vision.image.model.Pixel;
import com.epam.vision.image.context.ImageContext;
import com.epam.vision.image.context.MaskAnnotationContext;
import com.epam.vision.image.util.CIELab;
import com.epam.vision.image.util.DeltaE;
import com.epam.vision.image.util.DeltaEType;
import java.awt.Color;
import java.awt.Point;
import java.awt.Shape;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.Queue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Serhii Ilnytskyi
 * @version 1.0
 * @since 30.11.2019
 */

public class FloodFillStage implements Stage<MaskAnnotationContext, ImageContext> {

  private static final Logger logger = LogManager.getLogger(FloodFillStage.class);

  private static final CIELab CIELAB_CONVERTER = CIELab.getInstance();
  private static final Color DEFAULT_COLOR = new Color(150, 20, 100);
  private static final double DELTA_MAX = 90.00;
  private static final double L_WEIGHT = 0.81096;
  private static final double C_WEIGHT = 0.08765;
  private static final double H_WEIGHT = 0.10139;

  private DeltaE deltaEInstance;
  private Queue<Pixel> pixels = new LinkedList<>();
  private float deltaL;
  private float deltaa;
  private float deltab;

  @Override
  public ImageContext process(MaskAnnotationContext input) throws StageException {
    pareProcessInitialization(input);
    Shape shape = input.getImageAnnotation().getShape();
    logger.info("Flood filling. Please wait... ");
    while (pixels.size() > 0) {
      Pixel pixel = pixels.poll();

      double deltaE = deltaEInstance.calculate(pixel.getCieLab(), input.getAverageColorCieLab());

      if (!shape.contains(pixel.getX(), pixel.getY()) && (deltaE
          > DELTA_MAX)) { //exactly all inside shape
        continue;
      }

      verifyPixel(input, pixel.getX() + 1, pixel.getY());
      verifyPixel(input, pixel.getX() - 1, pixel.getY());
      verifyPixel(input, pixel.getX(), pixel.getY() + 1);
      verifyPixel(input, pixel.getX(), pixel.getY() - 1);

      changeColor(input.getImage(), pixel);
    }
    logger.info("Mask color changed.");
    return new ImageContext(input.getImage());
  }

  private void pareProcessInitialization(MaskAnnotationContext input) {
    logger.info("Initializing flood fill algorithm");
    pixels.addAll(input.getPixels());

    DeltaEType deltaEType = DeltaEType.DELTA_E_2000;

    deltaEType.setWeights(L_WEIGHT, C_WEIGHT, H_WEIGHT);
    deltaEInstance = new DeltaE(deltaEType);

    Color expectedColor = DEFAULT_COLOR;
    float[] expectedCieLab = CIELAB_CONVERTER.fromRGB(expectedColor.getRGB());
    deltaL = expectedCieLab[0] - input.getAverageColorCieLab()[0];
    deltaa = expectedCieLab[1] - input.getAverageColorCieLab()[1];
    deltab = expectedCieLab[2] - input.getAverageColorCieLab()[2];
  }

  private void verifyPixel(MaskAnnotationContext input, int x, int y) {
    BufferedImage image = input.getImage();

    Point point = new Point(x, y);
    int rgb = image.getRGB(x, y);
    float[] cieLab = CIELAB_CONVERTER.fromRGB(rgb);
    Pixel pixel = new Pixel(point, cieLab);

    if (input.getPixels().contains(pixel)) {
      return;
    }
    input.getPixels().add(pixel);
    pixels.offer(pixel);
  }

  private void changeColor(BufferedImage image, Pixel pixel) {
    int x = pixel.getX();
    int y = pixel.getY();

    float[] cieLabPixelColor = pixel.getCieLab();
    float newL = cieLabPixelColor[0] + deltaL;
    float newa = cieLabPixelColor[1] + deltaa;
    float newb = cieLabPixelColor[2] + deltab;

    newL = Math.max(0, Math.min(100, newL));
    newa = Math.max(-128, Math.min(128, newa));
    newb = Math.max(-128, Math.min(128, newb));
    float[] rgb = CIELAB_CONVERTER.toRGB(new float[]{newL, newa, newb});
    int resultedRGB = (int) (rgb[0] * 256);
    resultedRGB = (resultedRGB << 8) + (int) (rgb[1] * 256);
    resultedRGB = (resultedRGB << 8) + (int) (rgb[2] * 256);
    image.setRGB(x, y, resultedRGB);
  }
}
