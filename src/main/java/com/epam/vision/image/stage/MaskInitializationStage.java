package com.epam.vision.image.stage;

import com.epam.vision.image.annotations.ImageAnnotation;
import com.epam.vision.image.annotations.Mask;
import com.epam.vision.image.context.AnnotationContext;
import com.epam.vision.image.context.MaskAnnotationContext;
import com.epam.vision.image.model.Pixel;
import com.epam.vision.image.util.CIELab;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Serhii Ilnytskyi
 * @version 1.0
 * @since 30.11.2019
 */

public class MaskInitializationStage implements
    Stage<MaskAnnotationContext, MaskAnnotationContext> {

  private static final Logger logger = LogManager.getLogger(MaskInitializationStage.class);
  private static final CIELab CIELAB_CONVERTER = CIELab.getInstance();

  @Override
  public MaskAnnotationContext process(MaskAnnotationContext input) throws StageException {
    ImageAnnotation annotation = input.getImageAnnotation();
    if (!(annotation instanceof Mask)) {
      throw new StageException(new ClassCastException());
    }
    logger.info("Started Mask annotation initialization");
    MaskAnnotationContext maskContext = new MaskAnnotationContext(input.getImage(),
        (Mask) input.getImageAnnotation());

    BufferedImage image = maskContext.getImage();
    Shape shape = maskContext.getImageAnnotation().getShape();
    Set<Pixel> pixels = maskContext.getPixels();

    float averageL = 0;
    float averagea = 0;
    float averageb = 0;

    Rectangle bounds = shape.getBounds();

    for (int i = bounds.x; i < (bounds.width + bounds.x); i++) {
      for (int j = bounds.y; j < (bounds.height + bounds.y); j++) {
        Point2D point2D = new Point(i, j);
        if (shape.contains(point2D)) {
          int rgb = image.getRGB(i, j);
          float[] cieLab = CIELAB_CONVERTER.fromRGB(rgb);
          Pixel pixel = new Pixel(point2D, cieLab);
          averageL += cieLab[0];
          averagea += cieLab[1];
          averageb += cieLab[2];
          pixels.add(pixel);
        }
      }
    }
    averageL /= pixels.size();
    averagea /= pixels.size();
    averageb /= pixels.size();

    maskContext.setAverageColorCieLab(new float[]{averageL, averagea, averageb});
    logger.info("Ended Mask annotation initialization");
    return maskContext;
  }
}
