package com.epam.vision.image.annotations;

import java.awt.Color;
import java.awt.Shape;
import java.util.Objects;

public class ExpectedColor implements ImageAnnotation {
    public static final String ANNOTATION_NAME = "ecolor";


    private Color color;

    public ExpectedColor(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ExpectedColor that = (ExpectedColor) o;
        return Objects.equals(color, that.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color);
    }

    @Override
    public String getName() {
        return ANNOTATION_NAME;
    }

    @Override
    public String toString() {
        return "Color{" +
                "color=" + color +
                '}';
    }
}
