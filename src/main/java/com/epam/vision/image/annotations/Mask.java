package com.epam.vision.image.annotations;

import java.awt.*;
import java.util.Objects;

public class Mask implements ImageAnnotation {
    public static final String ANNOTATION_NAME = "mask";
    private Shape shape;
    private Color maskColor;

    public Mask() {
    }

    public Mask(Shape shape) {
        this.shape = shape;

    }

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mask that = (Mask) o;
        return shape.getBounds().equals(that.shape.getBounds());
    }

    @Override
    public String getName() {
        return ANNOTATION_NAME;
    }

    public Color getMaskColor() {
        if (maskColor == null) {
            maskColor = new Color(0, 255, 0, 255);
        }
        return maskColor;
    }

    public void setMaskColor(Color maskColor) {
        this.maskColor = maskColor;
    }


    @Override
    public int hashCode() {
        return Objects.hash(shape);
    }

    @Override
    public String toString() {
        return "Mask{" +
                "shape=" + shape +
                '}';
    }
}
