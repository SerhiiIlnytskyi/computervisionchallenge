package com.epam.vision.image.annotations;

import java.awt.*;
import java.util.Objects;

public class BoundingBox implements ImageAnnotation {
    public static final String ANNOTATION_NAME = "bbox";
    private int xMin;
    private int yMin;
    private int xMax;
    private int yMax;
    private Color rectColor;

    public BoundingBox() {
    }

    public BoundingBox(int xMin, int yMin, int xMax, int yMax) {
        this.xMin = xMin;
        this.yMin = yMin;
        this.xMax = xMax;
        this.yMax = yMax;
    }

    public int getxMin() {
        return xMin;
    }

    public void setxMin(int xMin) {
        this.xMin = xMin;
    }

    public int getyMin() {
        return yMin;
    }

    public void setyMin(int yMin) {
        this.yMin = yMin;
    }

    public int getxMax() {
        return xMax;
    }

    public void setxMax(int xMax) {
        this.xMax = xMax;
    }

    public int getyMax() {
        return yMax;
    }

    public void setyMax(int yMax) {
        this.yMax = yMax;
    }

    @Override
    public String getName() {
        return ANNOTATION_NAME;
    }

    public Color getRectColor() {
        if (rectColor == null) {
            rectColor = Color.red;
        }
        return rectColor;
    }

    public void setRectColor(Color rectColor) {
        this.rectColor = rectColor;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BoundingBox that = (BoundingBox) o;
        return xMin == that.xMin &&
                yMin == that.yMin &&
                xMax == that.xMax &&
                yMax == that.yMax;
    }

    @Override
    public int hashCode() {
        return Objects.hash(xMin, yMin, xMax, yMax);
    }

    @Override
    public String toString() {
        return "BoundingBox{" +
                "xMin=" + xMin +
                ", yMin=" + yMin +
                ", xMax=" + xMax +
                ", yMax=" + yMax +
                '}';
    }
}
