package com.epam.vision.image.annotations;


public interface ImageAnnotation {

    String getName();

}
