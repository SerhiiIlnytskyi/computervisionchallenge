package com.epam.vision.image.pipeline;

import com.epam.vision.image.context.ImageContext;
import com.epam.vision.image.stage.Stage;
import com.epam.vision.image.stage.Stage.StageException;

public class Pipeline<I extends ImageContext, O extends ImageContext> {
  private final Stage<I, O> current;
  public Pipeline(Stage<I, O> current) {
    this.current = current;
  }

  public  <NewO extends ImageContext> Pipeline<I, NewO> pipe(Stage<O, NewO> next) {
    return new Pipeline<>(input -> next.process(current.process(input)));
  }

  public O execute(I input) throws StageException {
    return current.process(input);
  }


}
