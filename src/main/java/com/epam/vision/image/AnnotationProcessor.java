package com.epam.vision.image;

import com.epam.vision.image.annotations.ImageAnnotation;
import java.awt.image.BufferedImage;

public interface AnnotationProcessor {

  boolean canBeApplied(ImageAnnotation annotation);

  void apply(BufferedImage image, ImageAnnotation annotation);
}
