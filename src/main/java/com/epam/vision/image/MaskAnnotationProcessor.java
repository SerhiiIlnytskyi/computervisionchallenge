package com.epam.vision.image;

import com.epam.vision.image.annotations.ImageAnnotation;
import com.epam.vision.image.annotations.Mask;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Objects;

public class MaskAnnotationProcessor implements AnnotationProcessor {
    @Override
    public boolean canBeApplied(ImageAnnotation annotation) {
        return annotation instanceof Mask;
    }

    @Override
    public void apply(BufferedImage img, ImageAnnotation annotation) {
        Objects.requireNonNull(img, "Image is required");
        Mask maskAnnotation = (Mask) annotation;
        Graphics2D g2d = img.createGraphics();
        g2d.setColor(maskAnnotation.getMaskColor());
        g2d.fill(maskAnnotation.getShape());
        g2d.dispose();
    }
}
