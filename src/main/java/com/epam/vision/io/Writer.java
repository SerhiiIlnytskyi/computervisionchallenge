package com.epam.vision.io;

import com.epam.vision.image.AnnotatedImage;

import java.io.OutputStream;

public interface Writer {

    void write(AnnotatedImage image, OutputStream out);
}
