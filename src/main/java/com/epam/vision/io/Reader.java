package com.epam.vision.io;

import com.epam.vision.image.AnnotatedImage;

import java.io.InputStream;

public interface Reader {
    AnnotatedImage read(InputStream in);
}
