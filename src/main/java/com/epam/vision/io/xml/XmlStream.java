package com.epam.vision.io.xml;

import com.epam.vision.image.AnnotatedImage;
import com.epam.vision.image.annotations.BoundingBox;
import com.epam.vision.image.annotations.ExpectedColor;
import com.epam.vision.image.annotations.Mask;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.security.AnyTypePermission;
import com.thoughtworks.xstream.security.NoTypePermission;
import com.thoughtworks.xstream.security.NullPermission;
import com.thoughtworks.xstream.security.PrimitiveTypePermission;

import java.util.Collection;

abstract class XmlStream {

    private XStream xstream;

    protected XStream xstream() {
        if (null == xstream) {
            xstream = new XStream();
            xstream.addPermission(AnyTypePermission.ANY);
            xstream.addPermission(NoTypePermission.NONE);
            xstream.addPermission(NullPermission.NULL);
            xstream.addPermission(PrimitiveTypePermission.PRIMITIVES);
            xstream.allowTypeHierarchy(Collection.class);
            xstream.allowTypesByWildcard(new String[]{
                    "com.epam.**",
                    "java.awt.geom.**",
                    "sun.awt.geom.**",
                    "java.awt.**"
            });

            xstream.alias("image", AnnotatedImage.class);
            xstream.alias("bbox", BoundingBox.class);
            xstream.alias("mask", Mask.class);
            xstream.alias("ecolor", ExpectedColor.class);

        }
        return xstream;
    }
}
