package com.epam.vision.io.xml;

import com.epam.vision.io.Reader;
import com.epam.vision.image.AnnotatedImage;

import java.io.InputStream;

public class XmlReader extends XmlStream implements Reader {
    @Override
    public AnnotatedImage read(InputStream in) {
        return (AnnotatedImage) xstream().fromXML(in);
    }
}
