package com.epam.vision.io.xml;

import com.epam.vision.io.Writer;
import com.epam.vision.image.AnnotatedImage;

import java.io.OutputStream;

public class XmlWriter extends XmlStream implements Writer {
    @Override
    public void write(AnnotatedImage image, OutputStream out) {
        xstream().toXML(image, out);
    }
}
