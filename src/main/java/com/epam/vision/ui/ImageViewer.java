package com.epam.vision.ui;


import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class ImageViewer extends JPanel {
    private final BufferedImage image;


    public ImageViewer(BufferedImage image) {
        this.image = image;
        setVisible(true);
    }


    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, this);

    }


    public void run() {
        JDialog frame = new JDialog();
        frame.setTitle("Image preview");
        frame.add(this);
        frame.setResizable(false);
        frame.setSize(image.getWidth(), image.getHeight());
        frame.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        frame.setModal(true);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
