package com.epam.vision;

import com.epam.vision.image.AnnotatedImage;
import com.epam.vision.image.AnnotationProcessor;
import com.epam.vision.image.BoundingBoxAnnotationProcessor;
import com.epam.vision.image.ChangeColorMaskAnnotationProcessor;
import com.epam.vision.image.ThreasholdingAnnotationProcessor;
import com.epam.vision.image.annotations.BoundingBox;
import com.epam.vision.image.annotations.ExpectedColor;
import com.epam.vision.image.annotations.ImageAnnotation;
import com.epam.vision.image.annotations.Mask;
import com.epam.vision.io.xml.XmlReader;
import com.epam.vision.ui.ImageViewer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.imageio.ImageIO;

import picocli.CommandLine;

@CommandLine.Command(name = "image processor")
public class ImageProcessor implements Callable<Integer> {

    private static final Logger logger = LogManager.getLogger(ImageProcessor.class);

    @CommandLine.Option(names = {"-a", "--annotation"}, description = "annotated img XML descriptor", required = true)
    String annotationXml;

    @CommandLine.Option(names = {"-o", "--output"}, description = "the archive file", required = true)
    String output;

    @CommandLine.Option(names = "--ui", description = "Show UI")
    boolean showUI;

    @CommandLine.Option(names = {"-h", "--help"}, usageHelp = true, description = "display a help message")
    private boolean helpRequested = false;


    public static void main(String[] args) {
        int exitCode = new CommandLine(new ImageProcessor()).execute(args);
        System.exit(exitCode);
    }

    private Map<String, AnnotationProcessor> registry;

    private Map<String, AnnotationProcessor> getRegistry() {
        if (registry == null) {
            registry = new HashMap<>();
//            registry.put(BoundingBox.ANNOTATION_NAME, new BoundingBoxAnnotationProcessor());
//            registry.put(Mask.ANNOTATION_NAME, new MaskAnnotationProcessor());
//            registry.put(Mask.ANNOTATION_NAME, new ColoredMaskAnnotationProcessor());
//            registry.put(BoundingBox.ANNOTATION_NAME, new ColoredMaskAnnotationProcessor());
//            registry.put(ExpectedColor.ANNOTATION_NAME, new ColoredMaskAnnotationProcessor());
//            registry.put(Mask.ANNOTATION_NAME, new ChangeColorMaskAnnotationProcessor());
//            registry.put(ExpectedColor.ANNOTATION_NAME, new ChangeColorMaskAnnotationProcessor());

                        registry.put(BoundingBox.ANNOTATION_NAME, new ThreasholdingAnnotationProcessor());

            //todo modify existing annotation processor or add new processor
        }
        return registry;
    }

    @Override
    public Integer call() {
        String format = output.substring(output.lastIndexOf('.') + 1);

        AnnotatedImage ai;
        try {
            ai = new XmlReader().read(new FileInputStream(annotationXml));
        } catch (FileNotFoundException e) {
            logger.error("Failed to load annotation file from {}", annotationXml, e);
            return CommandLine.ExitCode.USAGE;
        }
        logger.info("Original image: {}", ai.getImageFile());
        BufferedImage outputImage;
        try {
            outputImage = ImageIO.read(new File(ai.getImageFile()));
            for (ImageAnnotation annotation : ai.getAnnotations()) {
                AnnotationProcessor processor = getRegistry().get(annotation.getName());
                if (processor != null && processor.canBeApplied(annotation)) {
                    logger.info("Applying {} annotation...", annotation.getName());
                    processor.apply(outputImage, annotation);
                }
            }

//            outputImage = ai.applyAnnotations();


        } catch (IOException e) {
            logger.error("Failed to apply annotations", e);
            return CommandLine.ExitCode.SOFTWARE;
        }

        try {
            ImageIO.write(outputImage, format, new File(output));
            logger.info("Annotated image was saved to {}", output);
        } catch (IOException e) {
            logger.error("Failed to save annotated image", e);
            return CommandLine.ExitCode.SOFTWARE;
        }


        if (showUI) {
            ImageViewer viewer = new ImageViewer(outputImage);
            viewer.run();
        }

        return CommandLine.ExitCode.OK;
    }
}
