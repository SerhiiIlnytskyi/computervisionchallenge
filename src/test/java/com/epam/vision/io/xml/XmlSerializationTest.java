package com.epam.vision.io.xml;

import com.epam.vision.io.Reader;
import com.epam.vision.io.Writer;
import com.epam.vision.image.AnnotatedImage;
import com.epam.vision.image.annotations.BoundingBox;
import com.epam.vision.image.annotations.Mask;
import org.junit.jupiter.api.Test;

import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import static org.junit.jupiter.api.Assertions.*;

class XmlSerializationTest {

    @Test
    void writeReadTest() {

        Mask mask = new Mask(new Area(new Rectangle2D.Double(5, 5, 90, 40)));

        BoundingBox bbox = new BoundingBox(0, 0, 100, 234);

        AnnotatedImage original = new AnnotatedImage("/some-img.png");
        original.addAnnotation(bbox);
        original.addAnnotation(mask);

        Writer writer = new XmlWriter();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        writer.write(original, out);
        byte[] bytes = out.toByteArray();

        Reader reader = new XmlReader();
        AnnotatedImage restored = reader.read(new ByteArrayInputStream(bytes));

        assertEquals(original, restored);
    }
}
